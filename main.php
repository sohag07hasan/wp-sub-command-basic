<?php 

/*
 * Plugin Name: WP Sub Command
 * Author: Brian Oliver
 * Author uri: http://wpdeveloperpro.com
 * Plugin uri: http://wpdeveloperpro.com
 * Description: Increase your subscribers fast and improve your blog engagement with the interactive comments bar.
 * Version: 1.1.0
 * */

define("COMMENTBAR_FILE", __FILE__);
define("COMMENTBAR_DIR", dirname(__FILE__) . '/');
define("COMMENTBAR_URI", plugins_url('/', __FILE__));

include COMMENTBAR_DIR . 'classes/comment-bar.php';
global $commentbar;
$commentbar = new CommentBar();


?>
############          WP Sub Command         #######################
Increase your subscribers fast and improve your blog engagement with the interactive comments bar.


Features:
	1. Admin interface to set gradient css, button color, text color etc
	2. Admin interface to enable or disable specific features
	3. Affilate properties
	4. Integration of mostly used Autoresponders


How to Use:
1. Unzip the plugin.
2. Upload to the wp-content/plugins/ directory
3. Navigate plugins management page from wordpress admin page
4. Find the plguin "Wp Sub Command" and activate it.
5. Go to the "Wp Sub Command" options page (submenu under Settings menu) and choose suitable options
6. Go to page or post editing screen. Find a metabox "Comment Bar". Activate it and set a time dealy
	to be appeared.
	

Thank you for using Wp Sub Command. If you have any feedback feel free to ask 
http://wpdeveloperpro.com 
	 